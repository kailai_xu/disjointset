# /usr/bin/python

from matplotlib import pyplot as plt
import matplotlib
import numpy as np

class Node(object):
    def __init__(self,i):
        self.i = i
        self.p = i
        self.r = 0

    def find_root(self,nodes):
        y = self.i
        while nodes[y].i != nodes[y].p:
            y = nodes[y].p
        return nodes[y]

    def find_root_path_compression(self,nodes):
        y = self.i
        while nodes[y].i != nodes[y].p:
            y = nodes[y].p
        self.p = y; z=self.i
        while nodes[z].i != nodes[z].p:
            z = nodes[z].p
            nodes[z].p = y
        return nodes[y]


def link_by_rank(n1,n2):
    if n1.r<n2.r:
        n1.p = n2.i
    elif n1.r>n2.r:
        n2.p = n1.i
    else:
        n1.r += 1
        n2.p = n1.i


def union_by_rank(n1,n2,nodes):
    r1 = n1.find_root_path_compression(nodes)
    r2 = n2.find_root_path_compression(nodes)
    # print 'link %d %d'%(r1.i,r2.i)
    link_by_rank(r1, r2)


class ConnectedComponent(object):
    """docstring for ConnectedComponent"""
    def __init__(self, vertex_num,edges):
        self.vertex_num = vertex_num
        self.nodes = [None]*vertex_num
        for i in range(0,vertex_num):
            self.nodes[i] = Node(i)
        self.edges = edges
        self.component_list()

    def component_list(self):
        for u, v in self.edges:
            union_by_rank(self.nodes[u], self.nodes[v],self.nodes)

        self.component = [None]*self.vertex_num
        for i in range(0, self.vertex_num):
            self.component[i] = self.nodes[i].find_root_path_compression(self.nodes).i

        sets_label = list(set(self.component))
        self.sets = ['']*len(sets_label)
        for i in range(0,self.vertex_num):
            self.sets[sets_label.index(self.component[i])] += '%d '%i

        return self.sets

    def plot_raw(self):
        pts = np.random.rand(self.vertex_num,2)
        plt.plot(pts[:,0],pts[:,1],'o')
        for i,pt in enumerate(pts):
            plt.text(pt[0]+0.01,pt[1]+0.01,str(i))
        for u,v in self.edges:
            plt.plot((pts[u,0],pts[v,0]),(pts[u,1],pts[v,1]),'k')
        plt.show()




# test case

cc = ConnectedComponent(8, ((0,1),(0,2),(0,7),(3,5),(4,5),(2,6)))
cc.plot_raw()








